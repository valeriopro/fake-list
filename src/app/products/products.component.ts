import { Component, OnInit } from '@angular/core';
import { FetchProductsService } from '../services/fetch-products.service';
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  constructor(private fetchProducts : FetchProductsService) { }

  getProducts () {
    return this.fetchProducts;
  }

  ngOnInit(): void {
    this.fetchProducts.productsFetch();
  }

}
